from xpresso.ai.core.data.versioning.resource \
    import PachydermResource
from xpresso.ai.core.data.versioning.utils \
    import name_validity_check
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import BranchInfoException, CreateBranchException


class BranchManager(PachydermResource):
    """
    Manages branch and its resources on a pachyderm cluster
    """
    INPUT_BRANCH_NAME = "branch_name"
    OUTPUT_BRANCH_NAME = "branch_name"
    OUTPUT_BRANCH_HEAD = "head"

    def __init__(self, pachyderm_client):
        super().__init__()
        self.pachyderm_client = pachyderm_client

    def check_branch_existence(self, repo_name: str, branch_name: str):
        """
        checks if the branch exists in the specified repo or not

        :param repo_name:
            name of the repo
        :param branch_name:
            name of the branch to check if exists
        :return:
            bool: True or False
        """
        branch_list = self.list(repo_name)
        exist_status = False
        for branch in branch_list:
            if branch[self.OUTPUT_BRANCH_NAME] == branch_name:
                exist_status = True
                break
        if not exist_status:
            raise BranchInfoException(
                f"{branch_name} branch not found in {repo_name} repo"
            )

    def create(self, repo_name: str, branch_name: str):
        """
        creates a new branch in specified repo on pachyderm cluster

        :param repo_name:
                name of the repo
        :param branch_name:
                name of the branch
        :return:
        """
        name_validity_check(self.INPUT_BRANCH_NAME, branch_name)
        try:
            self.check_branch_existence(repo_name, branch_name)
            raise CreateBranchException(
                f"{branch_name} branch already exists in {repo_name} repo"
            )
        except BranchInfoException:
            self.pachyderm_client.create_new_branch(repo_name, branch_name)

    def list(self, repo_name: str):
        """
        returns a list of branches in specified repo

        :param repo_name:
            name of the repo to list the branches
        """
        branch_info = self.pachyderm_client.get_branch(repo_name)
        return self.filter_output(branch_info)

    def delete(self, repo_name, branch_name):
        """
        deletes a branch from the specified repo

        :param repo_name:
            name of the repo branch is in
        :param branch_name:
            name of the branch that needs to be deleted
        """
        self.check_branch_existence(repo_name, branch_name)
        self.pachyderm_client.delete_branch(repo_name, branch_name)

    def inspect(self, repo_name: str, branch_name: str):
        """
        fetches branch info from pachyderm cluster

        :param repo_name:
            name of the repo
        :param branch_name:
            name of the branch
        :return:
            dict with information on branch
        """
        # info is BranchInfo object
        branch_info = self.pachyderm_client.inspect_branch(repo_name, branch_name)
        # filter_output expects a list/generator
        output_info = self.filter_output([branch_info])
        return output_info[0]

    def filter_output(self, branch_info):
        """
        takes a branch info object and returns filtered output

        :param branch_info:
            information on the branch
        :return:
            list of branches filtered
        """
        branch_list = []
        for branch in branch_info:
            branch_data = {}
            branch_data[self.OUTPUT_BRANCH_NAME] = branch.name
            branch_data[self.OUTPUT_BRANCH_HEAD] = branch.head.id if branch.head.id else '-'
            branch_list.append(branch_data)
        return branch_list
